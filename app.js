///////////////////////////
// mostrando en pantalla //
///////////////////////////

const producto1 = 'pizza';
	  precio1 = 90;
	  producto2= 'hamburguesa';
	  precio2 = 20;

let html;

////////////////////////////////
// asi se usaba antiguamente  //
////////////////////////////////

// html = '<ul>'+
// 		'<li>Orden: ' + producto1 + '</li>'+
// 		'<li>Orden: ' + precio1 + '</li>'+
// 		'<li>Orden: ' + producto2 + '</li>'+
// 		'<li>Orden: ' + precio2 + '</li>'+
// 		'<li>total: ' + (precio1+precio2) + '</li>'+
// 		'</ul>';
// 		

//////////////////////////////////
// asi se usa en la actualidad  //
//////////////////////////////////
html = `
		<ul>

			<li>Orden:   ${producto1}  </li>
			<li>Precio:   ${precio1}    </li>
			<li>Orden:   ${producto2}  </li>
			<li>Precio:   ${precio2}    </li>
			<li>Total: ${total(precio1, precio2)}</li>

		</ul>

		`;

		function total(precio1, precio2)
		{
			return precio1 + precio2;
		}



		

		document.getElementById('app').innerHTML = html;